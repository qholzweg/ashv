<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="mobile-menu" id="<?php foundationpress_mobile_menu_id(); ?>" data-toggler="is-open" role="navigation">
	<a data-toggle="<?php foundationpress_mobile_menu_id(); ?>" class="close-button">
		<img src="<?= get_template_directory_uri() ?>/dist/assets/images/close-white.svg" alt="" class="close-icon">
	</a>
	<div class="menu-wrap">
		<a href="/">
			<img src="<?= get_template_directory_uri() ?>/dist/assets/images/logo.png" alt="" class="logo">
		</a>
		<?php foundationpress_mobile_nav(); ?>
		
	</div>
</nav>

<div class="off-canvas-content" data-off-canvas-content>
