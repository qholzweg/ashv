<?php 
add_shortcode( 'works_qty', 'works_qty_func' );

function works_qty_func( $atts ){
	$count_all = wp_count_posts( 'work' )->publish;
	$count_parents = count(get_posts(
		array(
			'post_type' => 'work',
			'post_parent' => 0, 
			 'post_status'=>'publish',
			 'posts_per_page'=>-1
		)
	));
	 return $count_all - $count_parents;
}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
	$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
	}
	add_filter( 'body_class', 'add_slug_body_class' );

	function mrb_filter_post_classes( $classes ) {
		global $post;
		if( has_post_thumbnail( $post->ID ) ) {
			$classes[] = "has-post-thumbnail";
		}
		return $classes;
	}
	add_filter( 'post_class', 'mrb_filter_post_classes' );