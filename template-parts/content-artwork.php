<?php 
$bg = CFS()->get( 'work_background' );

if (!empty($bg)) {
  $xl_bg_url = wp_get_attachment_image_url( $bg, 'fp-xlarge' );
  $l_bg_url = wp_get_attachment_image_url( $bg, 'fp-large');
  $m_bg_url = wp_get_attachment_image_url( $bg, 'fp-medium');
  $s_bg_url = has_post_thumbnail() ? 
    wp_get_attachment_image_url( $bg, 'fp-small') :
    wp_get_attachment_image_url( $bg, 'fp-medium');
}
elseif ( has_post_thumbnail( $post->post_parent ) ) { 
  $bg_id = $post->post_parent;
  $xl_bg_url = get_the_post_thumbnail_url( $bg_id, 'fp-xlarge' );
  $l_bg_url = get_the_post_thumbnail_url( $bg_id, 'fp-large');
  $m_bg_url = get_the_post_thumbnail_url( $bg_id, 'fp-medium');
  $s_bg_url = has_post_thumbnail() ? 
    get_the_post_thumbnail_url( $bg_id, 'fp-small') :
    get_the_post_thumbnail_url( $bg_id, 'fp-medium');
}

?>

<div class="line-wrapper">
    <div class="line"></div>
</div>
<div class="artwork container<?php if (!has_post_thumbnail() ) echo ' no-thumb';?>" id="main">
  <div class="artwork-wrapper">
    <div class="artwork-bg" data-interchange="[<?= $s_bg_url ?>, small], [<?= $m_bg_url ?>, medium], [<?= $l_bg_url ?>, large], [<?= $xl_bg_url ?>, xlarge]"></div>
		<a href="<?= $_GET['from_catalog'] ? '/catalog' :
get_post_permalink($post->post_parent); ?>" class="close-button">
			<img src="<?= get_template_directory_uri() ?>/dist/assets/images/close-white.svg" alt="" class="close-icon">
		</a>
		<?php if (has_post_thumbnail() ): 
      $xl_img_url = get_the_post_thumbnail_url( $post, 'fp-xlarge' );
      $l_img_url = get_the_post_thumbnail_url( $post, 'fp-large');
      $m_img_url = get_the_post_thumbnail_url( $post, 'fp-medium');
      $s_img_url = get_the_post_thumbnail_url( $post, 'fp-small');
      
      ?>
    <img
			data-interchange="[<?= $m_img_url ?>, small], [<?= $m_img_url ?>, medium], [<?= $l_img_url ?>, large], [<?= $xl_img_url ?>, xlarge]"
			alt="<?= the_title() ?>" class="artwork">
    <?php endif; ?>
	</div>

	<?php 
$fragments = CFS()->get( 'work_fragments' );
if (!empty($fragments)):
?>
	<div class="fragments">
		<?php if (count($fragments) <= 2) : ?>
		<div class="grid-x grid-padding-x align-center-middle text-center"  data-aos="fade-up">
			<?php foreach ( $fragments as $fragment ) : ?>
        <?php if (!empty($fragment) && $fragment['work_fragment'] !== ''): ?>
			<div class="cell small-6">
				<div class="mask"><img src="<?= wp_get_attachment_image_url($fragment['work_fragment'], 'full') ?>" alt=""></div>
			</div>
      <?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?php else: ?>
		<div class="hide-for-medium">
      <div class="grid-x grid-padding-x align-center-middle text-center">
        <div class="cell small-6" data-aos="fade-right"><div class="mask">
          <img src="<?= wp_get_attachment_image_url($fragments[0]['work_fragment'], 'full') ?>"
              alt="">
        </div></div>
        <div class="cell small-6" data-aos="fade-left"><div class="mask">
          <img src="<?= wp_get_attachment_image_url($fragments[1]['work_fragment'], 'full') ?>"
              alt="">
        </div></div>
      </div>
      <div class="grid-x grid-padding-x align-center-middle">
        <div class="cell small-6" data-aos="fade-up"><div class="mask">
          <img src="<?= wp_get_attachment_image_url($fragments[2]['work_fragment'], 'full') ?>"
              alt="">
        </div></div>
      </div>
    </div>
    
    <div class="show-for-medium grid-x grid-padding-x align-center-middle text-center">
      <div class="cell small-4" data-aos="fade-right"><div class="mask">
        <img src="<?= wp_get_attachment_image_url($fragments[0]['work_fragment'], 'full') ?>"
            alt="">
      </div></div>
      <div class="cell small-4" data-aos="fade-in"><div class="mask">
        <img src="<?= wp_get_attachment_image_url($fragments[1]['work_fragment'], 'full') ?>"
            alt="">
      </div></div>
      <div class="cell small-4" data-aos="fade-left"><div class="mask">
        <img src="<?= wp_get_attachment_image_url($fragments[2]['work_fragment'], 'full') ?>"
            alt="">
      </div></div>
    </div>
    
		<?php endif; ?>
	</div>
	<?php endif; ?>

	<div class="info" data-aos="fade-left">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell small-10 grid-x grid-padding-x">
					<div class="cell medium-4 title-block">
						<h1 class="title"><?= the_title() ?></h1>
						<?php if(CFS()->get( 'work_year' )) : ?><span class="year">'<?= CFS()->get( 'work_year' ) ?></span><?php endif; ?>
						<?php if(CFS()->get( 'work_materials' )) : ?><span class="materials"><?= CFS()->get( 'work_materials' ) ?></span><?php endif; ?>
						<?php if(CFS()->get( 'work_size' )) : ?><span class="size"><?= CFS()->get( 'work_size' ) ?>см</span><?php endif; ?>
              <span class="series">
              <?php the_terms( $post->ID, 'series', 'Серия ', ', ' ); ?>
              </span>
					</div>
					<div class="cell medium-8 descr"><?= the_content() ?></div>
				</div>
				<div class="cell small-2 nav"><?php get_template_part('template-parts/works_navigation') ?></div>
			</div>
		</div>
	</div>
</div>

<script>
window.onbeforeunload = function () {
  window.scrollTo(0, 0);
}
$(function(){
  'use strict';
  var $page = $('#main'),
      options = {
        debug: true,
        prefetch: true,
        cacheLength: 2,
        forms: 'form',
        anchors: 'a.navlink',
        onStart: {
          duration: 250, // Duration of our animation
          render: function ($container) {
            // Add your CSS animation reversing class
            $container.addClass('is-exiting');
            // Restart your animation
            smoothState.restartCSSAnimations();
          }
        },
        onReady: {
          duration: 0,
          render: function ($container, $newContent) {
            // Remove your CSS animation reversing class
            // Inject the new content
            var line = $('.line');
            line.removeClass('line');
            line.height();
            line.addClass('line');
            $container.html($newContent);
            $container.removeClass('is-exiting');
            $(document).foundation();
          }
        }
      },
      smoothState = $page.smoothState(options).data('smoothState');

});
</script>