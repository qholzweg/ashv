<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="main-container">
	<div class="main-grid  align-center-middle">
		<main class="main-content">
      <h1>Каталог</h1>
    <ul class='catalog-menu'>
    <?php 
    // this just adds from_catalog get value
    $My_Walker = new Ashv_Walker_Page();

    $args = array(
        'title_li'    => '',
        'depth'    => 2,
        'post_type'    => 'work',
        'walker'      => $My_Walker
    );
    wp_list_pages( $args );
    ?>
    </ul>



		</main>

	</div>
</div>

<?php get_footer();
