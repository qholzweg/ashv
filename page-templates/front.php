<?php
/*
Template Name: Front
*/
get_header('empty'); ?>

	<div class="front-content">
		<div class="logo-wrap">
		<img src="<?= get_template_directory_uri() ?>/dist/assets/images/logo.png" alt="" class="logo">
	</div>
	<div class="menu-wrap">
		<?php foundationpress_mobile_nav(); ?>
	</div>
	<div class="info">
		<?php the_content(); ?>
		<div class="text-center"><a href="/contacts#subsciption" class="button">Подписаться на обновления</a></div>
	</div>
	</div>

<?php get_footer('empty');
