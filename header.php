<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri() ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri() ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= get_template_directory_uri() ?>/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?= get_template_directory_uri() ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function () {
			if (!(document.body.classList.contains('page') && !document.body.classList.contains('page-template-front'))) {
				document.body.classList.add('is-loading');

				window.addEventListener("load", showPage);

				function showPage() {
					document.body.classList.remove('is-loading');
				}
			}
		});

	</script>
	<style>
		.is-loading *,
		.is-loading *:before,
		.is-loading *:after {
			animation-play-state: paused !important;
		}
		.is-loading {
			position: relative;
		}
		.is-loading:before {
			position: absolute;
			top: 0;
			left: 0;
			width: 100vw;
			height: 100vh;
			background: #192c3a;
			z-index: 990;
			content: '';
		}
		.is-loading:after {
			content: 'ЗАГРУЗКА...';
			font-family: 'Noto Sans', -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;;
			font-size: 16px;
			color: #B6CAE1;
			z-index: 999;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
		}
	</style>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<header class="site-header" role="banner">
		<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
			<div class="title-bar-right">
				<button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon" type="button"
					data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
			</div>
		</div>

	</header>
