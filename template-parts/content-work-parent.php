<?php 

$xl_img_url = get_the_post_thumbnail_url( $post, 'fp-xlarge' );
$l_img_url = get_the_post_thumbnail_url( $post, 'fp-large' );
$m_img_url = get_the_post_thumbnail_url( $post, 'fp-medium' );
$s_img_url = get_the_post_thumbnail_url( $post, 'fp-small' );

?>

<div class="parent container" id="main">
    <div class="parent-bg" data-interchange="[<?= $s_img_url ?>, small], [<?= $m_img_url ?>, medium], [<?= $l_img_url ?>, large], [<?= $xl_img_url ?>, xlarge]"></div>
    <div class="clouds-wrap">
  <?php 
  //get children
  $child_args = array(
    'post_type'      => 'work',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'DESC',
    'orderby'        => 'menu_order'
 );

$parent = new WP_Query( $child_args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

    <?php  
    $x = CFS()->get( 'work_img_x', $post->ID );
    $y = CFS()->get( 'work_img_y', $post->ID );
    $mx = CFS()->get( 'work_img_mob_x', $post->ID );
    $my = CFS()->get( 'work_img_mob_y', $post->ID );
     ?>
     <style>
         #work-<?php the_ID(); ?> {
            left: <?= empty($mx) ? $x : $mx; ?>%; 
            top: <?= empty($my) ? $y : $my; ?>%;
         }
         @media (min-width: 40em) {
            #work-<?php the_ID(); ?> {
                left: <?= $x ?>%; 
                top: <?= $y ?>%;
            }
        }
     </style>

        <div id="work-<?php the_ID(); ?>" class="artwork">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?= get_template_directory_uri() ?>/dist/assets/images/cloud.png" alt="" class="cloud">
            <span class="text-link"><?php the_title(); ?></span>
            </a>
        </div>
        

    <?php endwhile; ?>

<?php endif; wp_reset_postdata(); ?>
    </div>
<?php get_template_part('template-parts/set_navigation') ?>
</div>

<script>
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
$(function(){
  'use strict';
  var $page = $('#main'),
      options = {
        debug: true,
        prefetch: true,
        cacheLength: 2,
        forms: 'form',
        anchors: 'a.navlink',
        onStart: {
          duration: 250, // Duration of our animation
          render: function ($container) {
            // Add your CSS animation reversing class
            $container.addClass('is-exiting');
            // Restart your animation
            smoothState.restartCSSAnimations();
          }
        },
        onReady: {
          duration: 0,
          render: function ($container, $newContent) {
            // Remove your CSS animation reversing class
            // Inject the new content
            $container.removeClass('is-exiting');

            // Inject the new content
            $container.html($newContent);
            $(document).foundation();
          }
        }
      },
      smoothState = $page.smoothState(options).data('smoothState');

});
</script>