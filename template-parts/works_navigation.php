<?php

function getId($page) { return $page->ID; }  

$ancestors = get_post_ancestors( $post->ID );
$parent = ($ancestors) ? $ancestors[0] : null;

if ($parent) :
   $siblingIds = array_map( "getId", get_posts(
      array(
         'post_type'      => 'work',
         'posts_per_page' => -1,
         'post_parent'    => $parent,
         'order'          => 'ASC',
         'orderby'        => 'menu_order'
      )
   ) );

   $current = array_search( get_the_ID(), $siblingIds );
   $prevId = ( isset($siblingIds[$current-1]) ) ? $siblingIds[$current-1] : '';
   $nextId = ( isset($siblingIds[$current+1]) ) ? $siblingIds[$current+1] : '';
?>

<nav id="works-navigation">
    <?php if (!empty($prevId)) : ?>
      <div class="previous-set">
         <a 
           href="<?= get_permalink($prevId); ?>"
           title="<?= get_the_title($prevId); ?>" 
           class="previous-link navlink"
         ><img src="<?= get_template_directory_uri() ?>/dist/assets/images/prev.png" alt="" class="prev-arrow"></a>
      </div>
    <?php endif; ?>
   
    <?php if (!empty($nextId)) : ?>
       <div class="next-set">
          <a
             href="<?= get_permalink($nextId); ?>"
             title="<?= get_the_title($nextId); ?>"
             class="next-link navlink"
          ><img src="<?= get_template_directory_uri() ?>/dist/assets/images/next.png" alt="" class="next-arrow"></a>
       </div>
    <?php endif; ?>
</nav>

<?php endif; ?>