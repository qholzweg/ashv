<?php
 
function getId($post) { return $post->ID; }  

   $siblingIds = array_map( "getId", get_posts(array (
      'post_type'    => 'work',
      'post_parent'  => 0,
      'sort_column'  => 'menu_order',
      'sort_order'   => 'asc',
      'posts_per_page' => -1
   )) );
if (!empty($siblingIds)) :

   $current = array_search( get_the_ID(), $siblingIds );
   $prevId = ( isset($siblingIds[$current-1]) ) ? $siblingIds[$current-1] : '';
   $nextId = ( isset($siblingIds[$current+1]) ) ? $siblingIds[$current+1] : '';
?>

<nav id="set-navigation">
    <?php if (!empty($prevId)) : ?>
      <a 
        href="<?php  echo get_permalink($prevId); ?>"
        title="<?php  echo get_the_title($prevId); ?>" 
        class="previous-set nav-button navlink"
      ><img src="<?= get_template_directory_uri() ?>/dist/assets/images/prev.png" alt="" class="prev-arrow"></a>
    
    <?php endif; ?>
   
    <?php if (!empty($nextId)) : ?>
       <a
          href="<?php echo get_permalink($nextId); ?>"
          title="<?php  echo get_the_title($nextId); ?>"
          class="next-set nav-button navlink"
       ><img src="<?= get_template_directory_uri() ?>/dist/assets/images/next.png" alt="" class="next-arrow"></a>
   <?php else: ?>  
      <a
          href="<?php echo get_permalink($siblingIds[0]); ?>"
          title="<?php  echo get_the_title($siblingIds[0]); ?>"
          class="next-set nav-button navlink"
       ><img src="<?= get_template_directory_uri() ?>/dist/assets/images/reverse.png" alt="" class="next-arrow"></a> 
    <?php endif; ?>
</nav>

<?php endif; ?>