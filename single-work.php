<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

$is_parent = wp_get_post_parent_id($post);

if (!$is_parent) : 
  //set 
  get_header();
  get_template_part( 'template-parts/content', 'work-parent', $post ); 
  get_footer();

else:
  
  //artwork
  get_header('empty');
	get_template_part( 'template-parts/content', 'artwork', (array) get_post_thumbnail_id() ); 
  get_footer('empty');
 
endif;